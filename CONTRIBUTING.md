---
# See `man pandoc | grep "Metadata variables"`
title: Contributing
# These two are supplied their final value from `git describe`
subtitle: "*version:* [${PROJECT_VERSION}](${PROJECT_VERSION_URL})"
version: "${PROJECT_VERSION}"
date: "${PROJECT_VERSION_DATE}"
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
subject: Contribution guidelines
category: Contribution guidelines
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
---

<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>
SPDX-FileCopyrightText: 2021 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Contributing to Open Hardware Standards

**Welcome to the community :)**\
With this standard we aim to contribute to the infrastructure for open source hardware.
We feel, that's super important work and it often requires lots of patience, concentration and creativity.\
That works much better in a team. So happy to have you here. **You are very welcome!**

**General remark:**

1. You are all smart adults. If you don't find the specification you are looking for in this document, please ask others or do it to the best of your knowledge.
2. Open documentation is key. Issues, discussions, decisions and changes are public and always open for feedback. This way, it's hard to exclude perspectives from other stakeholders and documents are highly reviewed from an early stage on.
3. Thanks for adding value! Contstructive feedback, issue reporting, remarks, ideas… standards are the condensation of cumulated knowledge. We need **your** ingenuity to keep them alive and up-to-date. And we are here, ready to assist you when you have trouble joining the process.

## Roles

- admin, consortium lead, main writers (1…3 individuals; **maintainer** or **owner rights**)
- consortium (**developer rights**)
- community (**reporter rights**)

## Contribution flow

### Development Branch

Development takes place on the [`develop`](
https://gitlab.com/OSEGermany/OHS-3105/-/tree/develop) branch,
not on the standard branch (`master`).
So if you want to see the latest development,
or create merge requests,
please relate to the [`develop`](
https://gitlab.com/OSEGermany/OHS-3105/-/tree/develop) branch.

You should **never** commit to `master` directly, but to `develop`.
Once you, the developer, deems those new changes stable,
the current consoritum takes a final decision on those changes.
(Of course it's meaningful to update and ask consortium members
before you seek for a final decision :) ).
Approved changes are fast-forward merged from `develop` into `master`.

In this way, `master` only ever points to a commit already in `develop`
which keeps things consistent for several use cases. Diff visualisation
between the latest DIN SPEC 3105 and OHS 3105 is one of those use cases
(see [#47](https://gitlab.com/OSEGermany/OHS-3105/-/issues/47) for details).

The latest release is available under the branch [`latest`](
https://gitlab.com/OSEGermany/OHS-3105/-/tree/latest).

### Editing rights

As the project still mainly depends on the consortium lead, I feel it's a good
idea that the current consortium lead gets in touch personally with everyone
seeking for editing rights, so the lead knows more or less whom we've given the
rights to, which makes work more predictable for the lead :)

Requirements for:

- developer rights: anyone who
  - *a* - is intrinsically motivated to contribute,
  - *b* - has at least the minimum technical/editorial skills to do so and

       > Note:\
       > You absolutely do not need to be an expert in the domain of OSH,
       > but you must, in any case, be open for feedback from experts
       > _and proactively seek for it_ whenever needed.
  - *c* - integrates well into the rest of the team of editors
       (primarily on a personal level)
- maintainer rights:
  - *a+b+c* - (specifically harmonises with other maintainers)
  - *d* - longterm commitment

So if you really want editing rights
(and not just share your ideas in issues, attend meetings etc.)
_and_ don't know anyone of the group yet,
please [open an issue](
https://gitlab.com/OSEGermany/OHS-3105/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
and share 1…2 sentences about yourself
and how we can contact you.
Looking forward to meet you! :)

### Content changes

1. Any of the roles mentioned above may contribute via issues, merge requests or other tasks.
   1. If you feel that your suggestion may cause/need further discussion → please open an issue.
   2. If you're relatively sure that there's no further discussion needed → feel free to submit a merge request or edit directly.
2. All relevant discussions must be documented in this repo. If you had an issue discussed via mail, copy/summarise it in the corresponding issue/merge request or open a new one. This gitlab repo is _the_ point of reference for all relevant information that led to the content in the document.
3. Changes in the document happen via merge request.
4. Consortium decides on merge requests. Consortium lead is part of the consortium. Decisions must be taken in meetings, open to all consortium members. Meetings are moderated by consortium lead. As any relevant discussion, meeting minutes are published in this gitlab repo in MEETUS.MD. Decisions should be taken in consent with all consortium members. However, if not possible or feasible (consortium members not present in the meeting, hardliners, issues on a personal level etc.) the consortium lead may place a decision, even without the agreement of all consortium members. Consortium members are free to open a new branch with a document that fully reflects their perspective.
5. Document may be sent to DIN e.V. to transform it into an official standard. From there on, please follow DIN's contribution guidelines.
6. Only restrict/formalise what's definitely necessary. It's a pilot project.

### Author attribution

You contributed to the standard and want to appear in the official list of authors?
Of course! But still, there's a little bit of bureaucracy attached to this.\
In short:

1. Ask for developer rights. You'll be then entitled to make changes directly in
   the document. This will make you a co-author.
2. Add your name and organisation you're representing to the list of authors in
   the corresponding document.\
   **Important:** It must be an `OHS` document! Those are community forks of the
   DIN SPEC, hence we can add authors on our own will.
3. Once a new stable version of the `OHS 3105` is done, we can submit it to DIN
   e.V. requesting an update of DIN SPEC 3105.
4. The authors listed in the document will then form the new consortium of a so
   called "DIN SPEC Workshop". Submitting to DIN's procedure and rules is a
   prerequisite. This will most likely include signing a license agreement
   granting DIN e.V. any rights of use of the new `OHS 3105` including
   representing the whole authorship by `DIN e.V.` in attributions.
5. Congrationaltions! You will now be listed as official co-author in the
   foreword of the next DIN SPEC 3105.

### Issue management

1. Anyone may create an issue.
2. Any community or consortium member may label the issues accordingly so that specialist are encouraged to give input to the discussion.
3. Every consortium member is assigned to at least one label (and vice versa). These assignments should reflect the field of profession/interest of the consortium member and give a guideline for the responsibility of open issues. So if there's an unresolved issue and it carries your label, please have a look at it.

## Version number convention

Documents here are versioned in compliance with [Semantic Versioning 2.0.0](https://github.com/semver/semver). The following section is an extract of this definition.

Given a version number **MAJOR.MINOR.PATCH**, increment the:

- **MAJOR** version when backwards incompatible changes are made,
- **MINOR** version when functionality in a backwards compatible manner have been added,
- **PATCH** version when backwards compatible bug fixes are made.
- **PATCH** version _must_ be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.

**MINOR** version +must be incremented if new, backwards compatible functionality is introduced. It +must be incremented if any section/functionality is marked as deprecated. It _may_ include **PATCH** level changes. PATCH version +must be reset to 0 when **MINOR** version is incremented.

**MAJOR** version _must_ be incremented if any backwards incompatible changes are introduced. It _may_ also include **MINOR** and **PATCH** level changes. **PATCH** and **MINOR** version _must_ be reset to 0 when **MAJOR** version is incremented.

## Communication

- We still rely on eMail. However, we are open for suggestions.
- We generally meet online and [here](https://fairmeeting.net/OSHstandardization).
- Find the meeting minutes in this repos [MEETUS.md](MEETUS.md).
